#!/usr/bin/python3
#myservice_description: Prague Temp + mongo OR rpiHAT

#from urllib.request import urlopen
#import json

from pprint import pprint
import requests
import subprocess as sp
import json
import os

import datetime

APIKEY='d938f0fd13e05a0a1be4de627976d6e2'
print("i... requesting....")
print("i... requesting....")
print("i... requesting....")
r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Prague&APPID={}'.format(APIKEY) , timeout=10 )
temp=round(10*( r.json()['main']['temp']-273.15  ))/10
humi=round(10*( r.json()['main']['humidity']  ))/10
print("i... GOT")
pprint(r.json())
with open("/tmp/get_prague.json", "w") as f:
    json.dump( r.json() , f )



print("i... ",temp," deg C   humidity ",humi," %\n\n INSERT INTO MONGODB:")

#CMD="mymongo.py -w data -c prague -v temp={:.1f},humi={:.0f}".format( temp,humi)
#print("i... ",CMD)
#ok=False
#try:
#    #a=sp.check_call( CMD.split() )
#    #print(a)
#    ok=True
#except:
#    print("X... Couldnt enter in mongodb via mymongo.py")

if 1==1:
    #CMD="nohup /usr/local/bin/rpihat 0,{:0.0f},300   2>&1 >/dev/null &".format( temp )
    CMD="nohup /usr/local/bin/rpihat 0,{:0.0f},350  ".format( temp )
    print( CMD )
    #a=sp.check_call( CMD.split() )
    sp.Popen( CMD.split(),
              stdout=open('/dev/null','w'),
              stderr=open('/dev/null','w'),
#    )
              preexec_fn=os.setpgrp )
#              preexec_fn=os.setpgrp )
    print("i... rpihat message was  sent")
#apikey="key_redacted" # sign up here http://www.wunderground.com/weather/api/ for a key

#url="http://api.wunderground.com/api/"+apikey+"/conditions/q/UK/Basingstoke.json"
#meteo=urlopen(url).read()
#meteo = meteo.decode('utf-8')
#weather = json.loads(meteo)





####################################################################
####################################################################
####################################################################
from influxdb import InfluxDBClient
import socket # name of comp
MYHOSTNAME=socket.gethostname()

print("D... write infl================================================")

ips=[]
ips1=[]
ips2=[]
try:
    with open( os.path.expanduser("~/.myservice_discover8086") ) as f:
        ips1=f.readlines()
except:
    print("X... NO FILE ~/.myservice_discover8086 with automatic IPs")

print("D... looking at permanents")
try:
    with open( os.path.expanduser("~/.myservice_permanent8086") ) as f:
        ips2=f.readlines()
except:
    print("X... NO FILE ~/.myservice_permanent8086 with permanent IPs")

ips=ips1+ips2
ips=[ i.strip() for i in ips]
with open(os.path.expanduser("~/.influx_userpassdb") ) as f:
    creds=f.readlines()
creds=[ i.strip() for i in creds ]

for IP in ips:
    if len(IP)<7:quit()
    print("\nD... write infl --------------------------------------",IP)
    if IP[0].isdigit():
        print("D... ...  digits")
        client = InfluxDBClient(IP, 8086,creds[0],creds[1],creds[2],ssl=False, timeout=3)
    else:
        print("D... ...  names == ssl")
        client = InfluxDBClient(IP, 8086,creds[0],creds[1],creds[2],ssl=True, verify_ssl=False,timeout=8)
#        client = InfluxDBClient(IP, 8086,creds[0],creds[1],creds[2],ssl=True)
#    client = InfluxDBClient(IP, 8086,creds[0], creds[1], creds[2] )

    json_body = [ {"measurement":"weather"+MYHOSTNAME} ]
    #print( json_body )
    #json_body[0]["measurement"]=IP

    # what works in tellsens2
    timestamp=datetime.datetime.now().strftime("%s")
    timestamp=int(timestamp)*1000*1000*1000

    json_body[0]["fields"]={"time":timestamp,"prague_temp":temp,"prague_humi":humi}
#    json_body[0]["fields"]=
    print("D... json to ",IP,"...", json_body )
    try:
        client.write_points(json_body)
    except:
        print("X... maybe database test not found.....or something...")
