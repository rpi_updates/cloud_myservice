#!/usr/bin/python3
#myservice_description: read pocasi
import requests
import subprocess as sp
import re
import datetime as dt
import os # for expanduser
import subprocess as sp

import logging
from logzero import setup_logger,LogFormatter,colors
import sys
import time

log_format = '%(color)s%(levelname)1.1s... %(asctime)s%(end_color)s %(message)s'  # i...  format
LogFormatter.DEFAULT_COLORS[10] = colors.Fore.YELLOW ## debug level=10. default Cyan...
loglevel=1 #if args.debug==1 else 11  # all info, but not debug
formatter = LogFormatter(fmt=log_format,datefmt='%Y-%m-%d %H:%M:%S')
logfile=os.path.splitext( os.path.expanduser("~/")+os.path.basename(sys.argv[0]) )[0]+'.log'
logger = setup_logger( name="main",logfile=logfile, level=loglevel,formatter=formatter )#to 1-50



WHATSUPYES=False
MONGOYES=False
WHATSUPYES=True

#def save_breakout( ss ):
#    with open("/tmp/forecast_breakout.txt","a") as f:
#        f.write( ss + "\n")

def teploty( sss ):
    logger.debug("D... splitting "+ sss )
    wrds=sss.strip().split()
    if wrds[-1]=="m/s":
        wrds=wrds[1:]
    if wrds[1]=="do":
        a=0
        b=int(wrds[2])
    else:
        a,b=int(wrds[1]),int(wrds[3])
    if a>b:
        return b,a
    return a,b


def prepare_mongo( hft,a,b):
    CMD="mymongo.py -w data -c forecast -v \"t={},low={},high={}\"".format(hft,a,b)
    logger.info("MONGO command :\n           "+CMD)
    try:
        sp.check_output( CMD, shell=True )
    except:
        logger.error("mongo error")



def write2tmp( MSG ):
    with open("/tmp/forecast.log", "a") as f:
        f.write( MSG +"\n")
    ###CMD='{}.myservice/WARNING/telegram.py "{} {}"'.format( os.path.expanduser("~/"), now,MSG )
    #CMD='{}.myservice/WARNING/telegram.py "{}"'.format( os.path.expanduser("~/"), MSG )
    #logger.info(CMD)
    #try:
    #    sp.check_output( CMD, shell=True )
    #except:
    #    logger.error( "TELEGRAM COMMAND NOT OK:\n    " +CMD )
    ##CMD='{}.myservice/yowsup_my.py "{} {}"'.format( os.path.expanduser("~/"), now, i )
    ##if WHATSUPYES:sp.check_output( CMD, shell=True )
    ##print( "COMMAND OK ",CMD )

##############################################
#
#          MAIN
#
#############################################

now=dt.datetime.now().strftime("%Y%m%d_%H%M%S_%A")
out="/tmp/forecast2_chmi"+now+".txt"
outappe="/tmp/forecast2_chmi_breakout.txt"
WEB="http://portal.chmi.cz/files/portal/docs/meteo/om/inform/p_ppcr1.html"


#CMD="wget -q "+web+" -O "+out;


r = requests.get('{}'.format(WEB) )
r.encoding='windows-1250' # YES
#print( r.text )
#with open(out ,"w"   ) as f:
#    f.write( r.text )
#m = re.search(r'.*(pre).*(S).*(tuace:)', r.text, re.M|re.I )
m = re.search(r'(<pre>)([.\s\w\W]+)(<\/pre>)', r.text, re.MULTILINE ) # OK <PRE>
#print(m,m.group() )

#txt=.replace("\n\n","#")
with open(out ,"w"   ) as f:
    f.write( m.group()  )

a=m.group().split("\n\n")
for i in a:
    logger.info("##############################################################")
    logger.info(i)
logger.info("##############################################################")


#m = re.findall(r'(Situace:\n)([\w\W]+)(##)', txt ) # OK
hoursadd=dt.datetime.today().replace(hour=3,minute=0,second=0,microsecond=0)
h12=dt.timedelta(hours=12)
hoursadd=hoursadd+h12

##################################
# noc or not noc
nsitua=0  # make bar at new situation
for i in a:
    i=i.replace("\n"," ")
    if 'Situace' in i:
        logger.info("\n\n###SITUACE "+str(nsitua)+" "+i)
        sit=re.search(r'Situace:([\W\w]+)', i )
        with open( outappe,"a") as f:
            if nsitua==0:
                f.write( "----------------------------"+now+"\n" )
                write2tmp( sit.group() ) #######
            f.write( "{}\n".format( sit.group() ) )
        nsitua=nsitua+1

    if 'Počasí v noci' in i:
        logger.debug("\n\n###POCASI  NOC "+str(nsitua)+" "+str(hoursadd)+" "+i)
        temp=re.search(r'teploty (?:[\W\w]+?)(C)', i )
        a,b=teploty(temp.group())
        print("TEPLOTY NOC", a,b )
        hft=hoursadd.strftime("%Y-%m-%d %H:%M:%S")
        #with open( outappe,"a") as f:
        logger.debug( "{} {} {}    \n".format( hft, a,b ) )
        prepare_mongo( hft,a,b)
        hoursadd=hoursadd+h12

    # Situace can contain 'pocasi'
    if ('Počasí' in i) and (not 'v noci' in i) and (not 'Situace' in i):
        logger.info("\n\n###POCASI    "+str(nsitua)+" "+str(hoursadd)+i)
        if nsitua==1:  write2tmp( i )  # THERE WAS A STITUATION ALREADY
        temp=re.search(r'teploty (?:[\W\w]+?)(C)', i )
        vitr=re.search(r'(\w+) v\wtr (?:[\W\w]+)(m/s)', i )
        # group()  da vsechno
        # (?:...)  nejde do group(1) a dalsich...
        a,b,c,d=0,0,0,0
        if temp!=None:
            a,b=teploty(temp.group())
        else:
            logger.error("NO TEMPERATURES DECLARED")
        if vitr!=None:
            direct=vitr.group(1)
            c,d=teploty(vitr.group() )
        else:
            logger.error("NO VITR DECLARED")
        ###logger.debug("TEPLOTY    "+ a + b +"  vitr "+c+d+ direct)
        hft=hoursadd.strftime("%Y-%m-%d %H:%M:%S")
        #with open( outappe,"a") as f:
        logger.info( "{} {} {}     vitr {} {}    {}\n".format( hft, a,b ,c,d, direct ) )
        prepare_mongo( hft,a,b)
        logger.debug("TEPLOTY   "+  str(a) +" "+str(b) )
        hoursadd=hoursadd+h12

######################### insert with MyMongo.py
