#!/usr/bin/env bash
#myservice_description: update currency conv

currency_converter 1 EUR --to CZK
DIRNAME=`dirname $0`

#DIRNAME=#

cd  ${HOME}/.local/lib
cd `ls -1tr | grep python3* | tail -1`

#/python3.10/
cd site-packages/
cd currency_converter/
# eurofxref.csv
#cd "$DIRNAME/currency_converter"

rm -f eurofxref*

wget 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist.zip'
wget 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref.zip'
unzip 'eurofxref.zip' && rm 'eurofxref.zip'

currency_converter 1 EUR --to CZK
