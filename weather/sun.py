#!/usr/bin/env python3
#myservice_description: sun position
import datetime
from suntime import Sun, SunTimeException

from fire import  Fire

import time


from alive_progress import alive_bar


import notifator.notify as nt

minu=15
minute7 = 60*minu


def countdown( n ):
    n = int(n)
    with alive_bar( n ) as bar:
        for i in range(n):
            bar()
            time.sleep(1)


def warning(MSG):
    l = len(MSG)+2 + 2*8 + 4
    print()
    print("-"*l)
    print("|          {}          |".format(MSG) )
    print("_"*l)
    print("           reading:", MSG)
    nt.issue_sound( MSG )

def get_warning_sr(day = None):
    # SL
    global minu  # was/is duplicite
    latitude = 49.8645606
    longitude = 14.2400767

    sun = Sun(latitude, longitude)
    if day is None:
        today_sr = sun.get_local_sunrise_time()
        today_ss = sun.get_local_sunset_time()
        warn_sr=today_sr-datetime.timedelta(minutes=minu)
        warn_ss=today_ss-datetime.timedelta(minutes=minu)
        print(' Place:  SL - raise: {}   set: {} '.
              format(warn_sr.strftime('%H:%M'), warn_ss.strftime('%H:%M')))
    else:
        abd = datetime.datetime.now()+datetime.timedelta(days=1)
        warn_sr = sun.get_local_sunrise_time(abd)
        warn_ss = sun.get_local_sunset_time(abd)
        print(' Place:  SL - raise: {}   set: {}   - on {}'.
              format(warn_sr.strftime('%H:%M'),
                     warn_ss.strftime('%H:%M'),
                     abd.strftime("%Y-%m-%d")
              ))
    return warn_sr,warn_ss


def watch_time():
    for i in range(111):
        print("_"*50)
        sr,ss=get_warning_sr()
        tz_info = sr.tzinfo
        now=datetime.datetime.now(tz_info)

        now = now - datetime.timedelta(hours=0, minutes=0) # OVERRIDE
        print("NOW =    ",now)

        if now<sr:
            delta=sr-now
            sleep = delta.total_seconds()
            print("Watching for        RISE now, comming in", delta, sleep )
            MSG = "Východ slunce v "+sr.strftime("%H:%M")

        elif now<ss:
            delta=ss-now
            sleep = delta.total_seconds()
            print("Watching for        SET only, comming in", delta, sleep )
            MSG = "Západ slunce v "+ss.strftime("%H:%M")

        else:
            abd = now + datetime.timedelta(days=1)
            print("NEXT DAY :")
            sr,ss=get_warning_sr( abd )

            # midnight = datetime.datetime(now.year, now.month, now.day, 23, 59, tzinfo=tz_info)
            # delta = midnight - now
            # sleep = delta.total_seconds()
            # sleep = sleep + 60
            # print("Waiting  midnight, comming in", delta, sleep )
            # MSG = "Nextday "

            delta = sr - now
            sleep = delta.total_seconds()
            sleep = sleep
            print("Waiting next rise, comming in", delta, sleep )
            MSG = "Východ slunce v "+sr.strftime("%H:%M")


        # SLEEP SECTION
        if sleep>minute7:
            sleep = sleep - minute7
            MSG2 = MSG + " za 15 minut"
            asleep = minute7
        else:
            #sleep = 1
            MSG2 = MSG + " za méně než 15 minut"
            asleep = sleep

        print("D...      sleeping ", sleep)
        # sleep = 5 # OVERRIDE
        #time.sleep( sleep )
        countdown(sleep)
        warning( MSG2 )
        print("D... aftersleeping", asleep)
        countdown(asleep)
        warning( MSG )
        countdown(60)


if __name__=="__main__":
    watch_time()
    Fire({"warn":get_warning_sr,
          "watch":watch_time
    })

# # Get today's sunrise and sunset in UTC
# today_sr = sun.get_sunrise_time()
# today_ss = sun.get_sunset_time()
# print('SL -  \n - raise: {} and get down at {} UTC'.
#       format(today_sr.strftime('%H:%M'), today_ss.strftime('%H:%M')))

# today_sr = sun.get_local_sunrise_time()
# today_ss = sun.get_local_sunset_time()
# print('SL -  \n - raise: {} and get down at {} '.
#       format(today_sr.strftime('%H:%M'), today_ss.strftime('%H:%M')))


# # On a special date in your machine's local time zone
# # abd = datetime.date(2014, 10, 3)
# abd = datetime.datetime.now()+datetime.timedelta(days=1)

# abd_sr = sun.get_local_sunrise_time(abd)
# abd_ss = sun.get_local_sunset_time(abd)
# print('On {} the  \n - raise: {} and gets down at {}.'.
#       format(abd, abd_sr.strftime('%H:%M'), abd_ss.strftime('%H:%M')))

# # # Error handling (no sunset or sunrise on given location)
# # latitude = 87.55
# # longitude = 0.1
# # sun = Sun(latitude, longitude)
# # try:
# #     abd_sr = sun.get_local_sunrise_time(abd)
# #     abd_ss = sun.get_local_sunset_time(abd)
# #     print('On {} at somewhere in the north the sun raised at {} and get down at {}.'.
# #           format(abd, abd_sr.strftime('%H:%M'), abd_ss.strftime('%H:%M')))
# # except SunTimeException as e:
# #     print("Error: {0}.".format(e))
