#!/usr/bin/python3
#myservice_description: radar picture get
ACT="/tmp/radar_actual.png"
mask="/tmp/radarCzechrep.jpg"      # TEST PICTURE
testRain="/tmp/radarTestRain.png"  # TEST PICTURE

DEBUG=0



from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import PIL
import os
import subprocess as sp


sites={ 'mnisek':      [(287,316),8 ],#MNICH
        "rez":         [(293,284) ,8],#REZ
        "bratrikovice":[(300,350) ,8],#BRAT
        "snezka":      [ (398,220) ,8],  #SNEZKA
        "cr":          [ (298,315) , 4*40]  # praha+CR
        }





##########################
# ORIGINAL GET
#########################
def get_picture(dest='web'):
	rname='annotation_placeholder'
	if ( dest=='web'):
### GET last picture name
		a=os.popen('wget -q http://www.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/ -O - |  grep png | awk -F \\\"  \'{print $2 }\' | sort | tail -1')
		rname=a.readline()
		tmppng='/tmp/radar_'+rname.rstrip()
		print('i.... remote name=',tmppng)
		os.system('wget -c http://portal.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/'+ rname.rstrip() +' -O '+tmppng)
		os.system('cp '+tmppng+' /tmp/radar_actual.png')
		return tmppng
	if ( dest=='debug'):
		tmppng='i.png'
		os.system('wget -c http://portal.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/pacz23.z_max3d.20160917.1500.0.png -O '+tmppng)
		print('i.... remote name=',tmppng)
###os.system('cp i.png  '+tmppng)  ### jakoby wget
		return tmppng
	if ( dest=='test1'):
		tmppng='/tmp/test1.png'
		print('i.... remote name=',tmppng)
		return tmppng






###########################################
#
#        NEW STUFF
#
##########################################


def plot_circle( bg2,  draw , s ):
    """
    plots a rectangle (s is a list [(x,y),rectsize]
    returns zoom image size rectsize x rectsize
    """
    coor ,size=s[0],s[1]
    ltop=(coor[0]-int(size/2),coor[1]-int(size/2))
    rbot=(coor[0]+int(size/2),coor[1]+int(size/2))
    #print( ltop + rbot )
    inside=bg2.crop(  ltop+rbot )
    if size>16:return inside # CR NO PLOT
    draw.rectangle((  ltop , rbot ), fill="green")

    ltop=(coor[0]-int(size/4),coor[1]-int(size/4))
    rbot=(coor[0]+int(size/4),coor[1]+int(size/4))
    draw.rectangle((  ltop , rbot ), fill="red")

    ltop=(coor[0]-1,coor[1]-1)
    rbot=(coor[0]+1,coor[1]+1)
    draw.rectangle((  ltop , rbot ), fill="black")
    return inside







def evaluate_rain( zoom ):
    """
    computes the rains from the small picture
    Colors are taken manually
    factor is taken from logaritmic scale by ?interpolation?
    """
    rgb=[
	[56,0,112],
	[48,0,168],
	[0,0,252],
	[0,108,192],
	[0,160,0],
	[0,188,0],
	[52,216,0],
	[156,220,0],
	[224,220,0],
	[252,176,0],
	[252,132,0],
	[252,88,0],
	[252,0,0],
	[160,0,0],
	[252,252,252]
    ]
    factor=[0.056, 0.1, 0.178, 0.315, 0.56, 1.0, 1.78, 3.15, 5.6, 10., 17.8, 31.5, 56., 100., 178. ]
    intensity=[]
    size=zoom.size
    #for i in list(zoom.getdata()):
    #    print(i)
    for i in range(size[0]):
        for j in range(size[1]):
            border=""
            intpix=0
            color=zoom.getdata()[ i*size[1] +j ]
            if color[3]==0: continue
            if color==(255,0,255,255):
                border="border"
                continue           # skip border
            for r in range(len(rgb)):
                #print("         ",color[0:3],rgb[r])
                if list(color[0:3])==rgb[r]:
                    intpix=factor[r]
                    continue      # skip when FOUND
            if DEBUG!=0:print(i,j,  color, intpix, border)
            intensity.append( intpix )
    # # previsously i did compute with imagemagick.......
    # for i in range( len(rgb)):
    #     RGB='\"rgb('+','.join( map(str,rgb[i]))+')\"'
    #     CMD='convert '+tmpsample+' -fill black +opaque '+RGB+' -fill white -opaque '+RGB+' -format "%[fx:mean*'+str(factor[i])+']\\n" info: '
    #     #	CMD='convert '+tmpsample+' -fill black +opaque '+RGB+' -fill white -opaque '+RGB+' -format "%[fx:mean*'+str(1.0)+']\\n" info: '
    #     #print('ANA1...',CMD)
    #     a=os.popen(CMD)
    #     inten=a.readline().rstrip()
    #     intensity.append( float(inten) )
    #     print(i,inten)
    #print( intensity )
    if DEBUG!=0:print('i...  evaluate ... SUM=',sum(intensity))
    return int(10000*sum(intensity)/size[0]/size[1])/10000






###########################################
#
#  READ PICTURES
#
###########################################

ACT=get_picture('web')

img = Image.open( ACT ).convert('RGBA')
if DEBUG!=0:
    rain= Image.open( testRain ).convert('RGBA')
    mm_per_h=evaluate_rain( rain )
    print("+... avg rain intensity=",mm_per_h,"mm/hour")
    quit()


print(ACT, img.size)
if DEBUG!=0:
    bg  = Image.open( mask ).convert('RGBA')
    print(mask, bg.size)
    bg=bg.crop( (-35,-65,bg.size[0]+40,bg.size[1]-70)  )
    bg = bg.resize( img.size, PIL.Image.LANCZOS)
    print(mask, img.size)
    #img.paste( bg , (0, 0), bg )
    bg.paste( img , (0, 0),  img )
else:
    bg=img


###################################
#  RECTANGLES       RATHER before crop #####
####################################
bg2=bg.copy() # DUPLICATE BEFORE drawing
sitesZOOM={}  # this will contain zoom images
sitesRAINS={}  # this will contain zoom images
draw = ImageDraw.Draw( bg )
for s in sites.keys():
    zoom=plot_circle( bg2, draw, sites[s] )
    sitesZOOM[s]=zoom
    mm_per_h=evaluate_rain( zoom )
    sitesRAINS[s]=mm_per_h
    print("SITE: {:15s} {} mm/h".format(s,mm_per_h))
    zoom.save("/tmp/radarSite_"+s+".png")




mx=bg.size
#img2=img.crop( img.getbbox() )    #266x200+150+205
############  CROP  #############
img2=bg.crop( (140,200,mx[0]-280,mx[1]-160)  )
if DEBUG!=0: img2=bg.crop( (0,0,mx[0],mx[1])  ) # NOCROP
############   TEXT #############
draw = ImageDraw.Draw(img2)
#font = ImageFont.truetype("arial.ttf", 18) # cannot find
font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf", 14)
draw.text((0, 0), os.path.splitext(os.path.basename(ACT))[0] ,(255,255,255) , font=font)
############

#img2.show()
img2.save("/tmp/radar_actual2.png")


#CMD="mymongo.py -w data -c rains -v bratrikovice={},mnisek={},rez={},snezka={},cr={}".format( sitesRAINS['bratrikovice'],sitesRAINS['mnisek'],sitesRAINS['rez'],sitesRAINS['snezka'],sitesRAINS['cr'] )
#print(CMD)
#sp.check_call( CMD.split() )
#print("DONE ",CMD)



from influxdb import InfluxDBClient

print("D... write infl")

with open( os.path.expanduser("~/.myservice_discover8086") ) as f:
    ips=f.readlines()
ips=[ i.strip() for i in ips]
with open(os.path.expanduser("~/.influx_userpassdb") ) as f:
    creds=f.readlines()
creds=[ i.strip() for i in creds ]

for IP in ips:
    if len(IP)<7:quit()
    client = InfluxDBClient(IP, 8086,creds[0], creds[1], creds[2] , timeout=4)
    json_body = [ {"measurement":"weather"} ]
    #print( json_body )
    #json_body[0]["measurement"]=IP
    json_body[0]["fields"]={"rain_mnisek":sitesRAINS['mnisek'],
                            "rain_bratrikovice":sitesRAINS['bratrikovice'],
                            "rain_rez":sitesRAINS['rez'],
                            "rain_snezka":sitesRAINS['snezka'],
                            "rain_cr":sitesRAINS['cr'],
    }
#    json_body[0]["fields"]=
    print("D... json to ",IP,"...", json_body )
    client.write_points(json_body)
