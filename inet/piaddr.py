#!/usr/bin/python3
#myservice_description: change IP from hostname
################
#
#  #change the raspberry hostname
#
# NO sudo hostname pix2
# no change /etc/hosts  /etc/hostname
# nO sudo systemctl restart systemd-logind.service

# hostnamectl set-hostname pix2
#  sudo reboot
#
#  # and run this
################
# this is an object defined in inet - myservice2
import ap_switch as ap
import subprocess as s
import socket
import time

import os # env
import sys

import pandas as pd

from fire import Fire
import datetime as dt

def get_ap_name():
    wlan0 = "wlan0"
    wlan0 = ""
    #print(f"i... hard way - I ask for {wlan0}")
    #print("D... MY MODULE ap_switch MUST HAVE BEEN  imported")
    w=ap.pi3wifi( wlan0 )
    wlan0 = w.get_interface() # recover the name
    ap_name=w.get_current_ap()
    #print(f"i... current AP == {ap_name}")
    #time.sleep(5) # Should be 5
    return ap_name,wlan0




def read_table( name, nindex ):
    """
    Reading table from source file. Using /tmp and csv, returns df
    """

    print(f"i... reading {name} table from file:",__file__," ########")
    #print(__path__)
    res = []
    go = False


    with open(__file__) as f:
        tem = f.readlines()
    tem = [x.strip() for x in tem if x.find( "#TABLE_START".strip(" "))==0 ]
    #print(f"i... TOTAL {len(tem)} table starts identified")
    #print(tem)

    with open(__file__) as f:
        while True:
            tem = f.readline().strip()
            #print("i... tem:", tem)
            if tem.find( "#TABLE_START".strip(" ") )==0:
                print("i... ... ",tem)
                if tem.find(name)>0:
                    go = True
            if tem.find(  "#TABLE_STOP".strip(" ") )==0:
                if go: break
            if go:
                #print("i... ", tem)
                res.append(tem)

    res = [x for x in res if x.find("------")<0] # remove the breaks

    print(f"i... {len(res)} lines was read")
    if len(res)==0:return None

    OUTPY = "/tmp/piaddr.csv"
    with open( OUTPY, 'w' ) as f:
        for i in res:
            #print(i)
            f.write(i+"\n") # write csv with EOL

    #D = pd.read_csv( OUTPY, sep='\s*\|\s*', engine="python").iloc[:, 1:-1]
    df= pd.read_csv( OUTPY, sep='\s*\|\s*', engine="python", comment="#")
    df= df.set_index( nindex ) # piname , ap
    df.drop( df.columns[[0,-1]], axis=1 ,inplace=True)

    # postprocessing
    apcols = df.columns.tolist()
    for i in apcols:
        try:
            df[i] = df[i].str.replace("'","")
        except:
            continue

    return df



def check_route_iface( wlan0 ):
    """
    check if the routing is via WLAN interface
    """
    CMD="ip route show"
    res=s.check_output( CMD.split() ).decode('utf8').split('\n')

    res=[x.strip() for x in res if x.find('default')>=0 ]
    #if DEBUG:print( "D...",res )
    print("D... default routes number: ", len(res) )
    # primary try:
    interface = res[0].split()[4]
    # but search all
    for rouin in res:
        interfacex=rouin.split()[4]
        print("i... DEFAULT route interface tested: ", interfacex )
        if interfacex == wlan0:
            print(f"i... ... and it is a good INTERFACE")
            interface = wlan0
        else:
            print(f"i... ... and it is a BAD INTERFACE ... NOT", wlan0)
            #sys.exit(1)

    # if not wifi interface => do not do anything
    if interface.find("w")!=0:
        print(f"!... the interface is not starting with /w/  quitting")
        print(f"!... I cannot do the changes for {interface}")
        sys.exit(0)



def help():
    print("\ni... you (may) need to apt install wireless-tools\n")


def get_current_ip_if( interface, ):
    """
    get the address of the interface in question
    """
    allips = []
    CMD="ip addr show dev "+interface
    res=s.check_output( CMD.split() ).decode('utf8').split('\n')
    res=[x.strip() for x in res if x.find('inet ')>=0 ]  # not inet6
    for i in res:
        allips.append( i.split()[1].split("/")[0] )

    # print(res)
    # if len(res)>1: # if several ....
    #     res2=[x for x in res if x.find( refip )>=0 ]
    #     if DEBUG:print("D... multi ip: ",res2)
    #     if len(res2)>0: res=res2
    # ip=res[0].split()[1].split("/")[0]
    print( f"i... CURRENT IP of {interface} IS/ARE {allips}")
    return allips

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def main():
    """
    read the tables first
    """

    dfn = read_table("pinames", "piname")
    print(dfn)

    dfa = read_table("aps", "ap")
    print(dfa)

    ap_name,wlan0 = get_ap_name()
    #time.sleep(5) # Should be 5
    check_route_iface( wlan0 ) # EXIT INSIDE IF PROBLEM wlan0 REMAINS

    MYHOSTNAME=socket.gethostname()
    #MYHOSTNAME = "rpi3b4fa"
    ###############################################
    if MYHOSTNAME in dfn.index.values:
        print(f"i... {MYHOSTNAME} FOUND in the list, continuing IP change")
    else:
        print("x... /"+MYHOSTNAME+"/ not in the list of", len(dfn.index.values),"PC's")
        print(" ... quit")
        sys.exit(1)
    ##############################################


    # hostname in dfn;  test ap in dfa
    if ap_name in dfa.index.values:
        print("i... AP IS in the list ...")
    else:
        print("x... /"+ap_name+"/ not in the list of", len(dfa.index.values),"AP's")
        print(" ... quit")
        sys.exit(1)


    ips = get_current_ip_if(wlan0)

    # Combine prefix and sufix .... newaddress
    print("i... checking PRE/SUFIX")
    if ap_name.find("Android")==0:
        print("o... troublesome android... different prefix")
        prefix = ips[0].split(".")
        prefix = ".".join( prefix[:-1] )
    else:
        print("i... not Android, easy fixed prefix")
        prefix = dfa.loc[ ap_name, 'prefix' ]
    sufix = dfn.loc[ MYHOSTNAME, 'sufix' ]
    newaddr = prefix+"."+str(sufix)
    print("i... ",newaddr)


    # ips = get_current_ip_if(wlan0) # ABOVE...
    if newaddr in ips:
        print(f"i... {newaddr} already in {ips}")
    else:
        CMD="sudo ip addr add "+newaddr+"/24 dev "+wlan0
        print("_______________________________________________")
        print("X...", CMD )
        res=s.check_output( CMD.split() ).decode('utf8').split('\n')
        print(res)
        print("_______________________________________________")
        with open( os.path.expanduser("~/piaddr.log"),'a') as f:
            f.write( "\n".join(res)+"\n" )


    return
#=======================================================================




if __name__ == "__main__":
    with open( os.path.expanduser("~/piaddr.log"),'a') as f:
        f.write( f"#_start_ {dt.datetime.now()} _________________" )
        f.write( "\n")

    help()
    Fire(main)
    help()
    time.sleep(4) # to reset myservice


sys.exit(0)
#======================================================= OFFICIAL END =============================

"""
#TABLE_START aps
| ap                 | prefix        | comment  |
|--------------------+---------------+----------|
| AndroidAPF1AA      | '192.168.186' |  a32     |
| npi_cas_0          | '192.168.250' | gani     |
| NPICAS_m2          | '192.168.250' | gani     |
| ojrcarbon          | '192.168.250' | vadim    |
| MERCUSYS_8D88_5G   | '192.168.0'   | bratr    |
| MERCUSYS_8D88      | '192.168.0'   | bratr    |
| repky2             | '192.168.1'   | newrepky |
| dlink_DWR-933_8736 | '192.168.0'   | nevim    |
| NetOJR             | '192.168.1'   |          |
| drakula6           | '192.168.0'   | home     |
| drakula6-EXT       | '192.168.0'   | extender |
| OjrTR245G          | '192.168.0'   | tr24     |
| OjrTR24            | '192.168.0'   | tr24     |
#TABLE_STOP
"""


"""
#TABLE_START pinames
| piname   | sufix |
|----------+-------|
| rpi3ba0f |    15 |
| rpi3b20a |    18 |
| rpi3bac2 |    19 |
| rpi3b6b6 |    20 |
| rpi3b817 |    21 |
| rpi3b6d3 |    22 |
| rpi3b2c6 |    23 |
| rpi3bb19 |    24 |
| rpi4b456 |    90 |
| rpi4bee1 |    91 |
| rpi4b71d |    92 |
| rpi4b992 |    93 |
| rpi4b3c1 |    94 |
| rpi3bd87 |    30 |
| rpi3b762 |    31 |
| rpi3bee4 |    32 |
| rpi3b0ea |    33 |
| rpi3ba75 |    34 |
| rpi3b4fa |    35 |
| rpi3b9f4 |    38 | 
| rpi03a1  |    70 |
| rpi0c01  |    71 |
| rpi01c6  |    72 |
| rpi0dcb  |    73 |
| rpi05d8  |    74 |
| rpi0d44  |    75 |
| rpi4b068 |    95 |
| rpi4b064 |    96 |
| rpi4beab |    97 |
| rpi4ba39 |    98 |
#TABLE_STOP
"""
