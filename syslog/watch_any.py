#!/usr/bin/python3
#myservice_description: watch ports
#######################
#  WATCH ANY SYSLOG FILE
#  NOW
#    -   syslog for PORTS
#    -   auth   for sshd lines
#######################
#

with open("/tmp/watchany.log", "a") as f:
    f.write("starting watchany\n")

import logging
from logzero import setup_logger,LogFormatter,colors
import argparse
import os,sys

import subprocess as s
import datetime
import time

import re # auth


import inotify  # rotation of logs need a watch
import time
import traceback
import threading
import inotify.adapters

ALARM_SENTENCE="ACCESS ATTEMPT"


#https://stackoverflow.com/questions/3290292/read-from-a-log-file-as-its-being-written-using-python
#http://www.dabeaz.com/generators/Generators.pdf

#-------------------------  deleted and again, correctly..........
# iptables -I INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT 22: "
# iptables -I INPUT -p tcp --dport 8888 --syn -j LOG --log-prefix "ACCESS ATTEMPT 8888: "
# iptables -I INPUT -p tcp --dport 25000 --syn -j LOG --log-prefix "ACCESS ATTEMPT 25000: "
# iptables -I INPUT -p tcp --dport 25006 --syn -j LOG --log-prefix "ACCESS ATTEMPT 25006: "
#  and put into /etc/r.local
######################################
#
#    isend-notify  stuff
#
#####################################


import notify2
import time

def note(mess, coll=""):
    with open("/tmp/watchany.log", "a") as f:
        f.write("starting watchany note "+ datetime.datetime.now().strftime("%H:%M:%S")+"\n")

    notify2.init("initialization of notify2")
    with open("/tmp/watchany.log", "a") as f:
        print("starting watchany note2 "+ datetime.datetime.now().strftime("%S")+"\n")
    notice = notify2.Notification( mess )
    notice.show()
    # can be better
    #notice.Notification(message="watch_any.py", icon="/usr/share/icons/oxygen/base/32x32/actions/address-book-new.png" ,summary=mess).show()

    time.sleep(2)




########################################
#
#    FOLLOW .......  SAME FOR ALL FILES
#
########################################
def follow(syslog_file):
    """
    not used now.... original version without test of rotation
    """
    syslog_file.seek(0,2) # Go to the end of the file
    while True:
        line = syslog_file.readline()
        if not line:
            time.sleep(0.2) # Sleep briefly
            continue
        yield line



##################################################################
def process(line, history=False):
    """
    useless
    """
    return
#  if history:
#    logger.debug( '=' + line.strip('\n'))
#  else:
#    logger.debug( '>' + line.strip('\n'))
##################################################################




def follow2( syslog_file ):
    file_opened=False
    from_beginning = False
    notifier = inotify.adapters.Inotify()
    while True:  # 1st passage ever
        #try:
        #------------------------- check
        if not os.path.exists(syslog_file):
            logger.error( 'syslog_file does not exist')
            time.sleep(1)
            continue
        #logger.error( ' opening and starting to watch '+ syslog_file)
        #------------------------- open
        if not file_opened:
            file = open(syslog_file, 'r')
            logger.info( 'log - '+syslog_file+' - opened')

            if from_beginning:
                for line in file.readlines():
                    process(line, history=True)
                    yield line
        else:
            file.seek(0,2) # goto end of file
            from_beginning = True
        #------------------------- watch
        notifier.add_watch(syslog_file)  # ADDED THE FILE TO WATCH
        #try:
        for event in notifier.event_gen():  # this will loop over
            #try
            #logger.info("event: "+ str(event) )
            if event is not None:
                logger.info("event: "+ str(event) )

                (header, type_names, watch_path, filename) = event
                if set(type_names) & set(['IN_MOVE_SELF']): # moved
                    logger.error( 'syslog_file moved')
                    notifier.remove_watch(syslog_file)
                    file.close()
                    time.sleep(1)
                    #continue # continue leaves the watch stalled 2018-12-17
                    break # we break, because we need new file assesment??
                elif set(type_names) & set(['IN_MODIFY']): # modified
                    for line in file.readlines():
                        process(line, history=False)
                        yield line
#        except:
#            notifier.remove_watch(syslog_file)
#            logger.error( 'Notifier.remove_watch - from exception')
#            file.close()
#            time.sleep(1)
            #-------------------------
        #except (KeyboardInterrupt, SystemExit):
        #  break
        #except inotify.calls.InotifyError:
        #  time.sleep(1)
        #except IOError:
        #  time.sleep(1)
        #except:
        #  traceback.print_exc()
        #  time.sleep(1)

##################################################################





if __name__=="__main__":

    ####################################
    # PARSER ARG
    ######################################
    parser=argparse.ArgumentParser(description="""
    ------------------------------------------------------------------
     A snippet to watch events defined in iptables
    ------------------------------------------------------------------
    iptables -D INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT SSH"
       ...  defines the log sentence ....
    iptables -L INPUT   ...  will list the things
    """,usage="""
    """,
     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-d','--debug', action='store_true' , help='')
    parser.add_argument('varlogfile', action="store", nargs="+") # nargs='+'
    args=parser.parse_args()




    ###########################################
    # LOGGING   - after AGR PARSE
    ########################################
    log_format = '%(color)s%(levelname)1.1s... %(asctime)s%(end_color)s %(message)s'  # i...  format
    LogFormatter.DEFAULT_COLORS[10] = colors.Fore.YELLOW ## debug level=10. default Cyan...
    loglevel=1 if args.debug==1 else 11  # all info, but not debug
    formatter = LogFormatter(fmt=log_format,datefmt='%Y-%m-%d %H:%M:%S')
    #logfile=os.path.splitext( os.path.expanduser("~/")+os.path.basename(sys.argv[0]) )[0]+'.log'
    logfile=os.path.splitext( os.path.expanduser("~/")+os.path.basename( args.varlogfile[0] ) )[0]+'.log'
    logger = setup_logger( name="main",logfile=logfile, level=loglevel,formatter=formatter )#to 1-50




    with open("/tmp/watchany.log", "a") as f:
        print("before real start "+ datetime.datetime.now().strftime("%S")+"\n")

    #####################################
    #
    #   REAL START =====================
    ##
    ####################################
    note( "{}".format( "watch started" ) , "green" )

    with open("/tmp/watchany.log", "a") as f:
        print("real start after note "+ datetime.datetime.now().strftime("%M:%S")+"\n")


    #logger.info("============:  "+sys.argv[0]+" STARTED")
    logger.info("============:  "+args.varlogfile[0]+" STARTED")


    #################################### SYSLOG -  OLDSTYLE
    #mylogfile = open( args.varlogfile[0] )
    #loglines = follow(mylogfile)
    ##sshlogfile = open("/var/log/auth.log")
    ##sshloglines = follow(sshlogfile)



    loglines=follow2( args.varlogfile[0]  )   # returns an object to follow
    with open("/tmp/watchany.log", "a") as f:
        f.write("starting watchany critical")

    #================================================== SYSLOG =======
    if args.varlogfile[0]=="/var/log/syslog":
        logger.info("SYSLOG: ... watching predefined ports")


        for i in loglines:   ################## all over it goes through follow2 ### LOOP
            if  (ALARM_SENTENCE in i):
                src=re.search( r'SRC=([\.\d]+)',  i)
                dst=re.search( r'DST=([\.\d]+)',  i)
                spt=re.search( r'SPT=([\.\d]+)',  i)
                dpt=re.search( r'DPT=([\.\d]+)',  i)
                if src==None:
                    print("Failure to find  SRC  IP")
                    logger.error( i )
                #else:
                #    message="{:12s} :{:5s} -> {:12s} :{:5s}".format( src.group(1),spt.group(1),dst.group(1),dpt.group(1) )
                #    logger.info( message )

                if src.group(1)=="127.0.0.1":
                    logger.debug(" ........... no action for 127.0.0.1")
                else:
                    logger.info(" ........... ALARM ......................")
                    print(i)
                    #logger.info( i )
                    message="{:12s} :{:5s} -> {:12s} :{:5s}".format( src.group(1),spt.group(1),dst.group(1),dpt.group(1) )
                    logger.info( "SYSLOG:"+message )
                    note( "S/{}".format( message ) , "red" ) ### NOTE

        quit()





    #===================================================================# AUTH.LOG
    elif args.varlogfile[0]=="/var/log/auth.log":
        logger.info("AUTH  : ... watching sshd events ")

        #====================== sshd============================
        # Accepted publickey for USER from 127.0.0.1
        # Received disconnect from 127.0.0.1
        # Disconnected from 127.0.0.1 port 45244
        #

        for i in loglines:
            if  (" sshd[" in i):
                # text is the 2nd part
                # get date:
                dnow=datetime.datetime.now()
                year=dnow.year
                #i have problem with the date string. First limit spaces
                i=i.strip()
                i=i.replace("    "," ") #4  5ok,6ok,7ok
                i=i.replace("   "," ")  #3
                i=i.replace("  "," ")   #2
                dat=" ".join( i.split(' ')[:3] )
                dat=dat.strip()
                print("Da... date text=/"+dat+"/ list:3=", i.split(' ')[:3])
                dat=dat.strip() # date decode is sometimes bad
                print("D0... date text=/"+dat+"/ replacing spaces")
                dat=dat.replace("  "," ")
                print("D1... date text=/"+dat+"/")
                dat=dat.replace("  "," ")
                print("D2... date text=/"+dat+"/ expected %Y %b %d %H:%M:%S")
                conv=True
                try:
                    ddd=datetime.datetime.strptime(str(dnow.year)+" "+dat, "%Y %b %d %H:%M:%S")
                except:
                    conv=False
                # %Y is already solved
                #if not conv:
                #    print("D3... %Y not found:date text=/"+dat+"/ expected %Y %b %d %H:%M:%S")
                #    ddd=datetime.datetime.strptime(str(dnow.year)+" "+dat, "%Y %b %d %H:%M:%S")
                print("D3... date reconstr=",ddd)
                dage=dnow-ddd
                print("D... age==",dage)
                if dage.total_seconds()>60:
                    print("D...   too old")
                    continue
                #
                print("RSAREPLACED=",i)
                txt=i.split(' sshd[')[-1]
                txt=" ".join( txt.split(': ')[1:] )
                txt = re.sub(r'RSA SHA256.+','', txt)

                print("D... TXT=", txt)
                #txt=txt.replace("onnected","")
                #txt=txt.replace("enticating","")
                #search IP
                src=re.search( r'([\d]+\.[\d]+\.[\d]+\.[\d]+)',  i)
                print("SEARCH IP :=", src)
                #"RSA SHA256"
                if (src==None) or (i.find("Received disconnect")>0):
                    print("no IP on the line OR rec.discon.\n",i)
                    #logger.error( i )
                else:
                    print( src.group(1)  )
                    print(i)
                    print(" ........... ALARM")
                    if txt.find("Disconnected from invalid")<0:
#                        note( "A/{}".format( txt ) , "red" )
                        #print("X...  sending NOTE:", txt)
                        #print("X...  sending NOTE:", txt)
                        #print("X...  sending NOTE:", txt)

                        note( "{}".format( txt ) , "red" )
                    logger.info( "AUTH  :"+ i )
        quit()



    # ===================================================== NOT SYSLOG NOT AUTHLOG
    else:
        logger.info("NOTHING ... watching nothing:"+" ".join(args.varlogfile) )
