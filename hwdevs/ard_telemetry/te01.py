#!/usr/bin/env python3
# Based on the DHTNew library - https://github.com/RobTillaart/DHTNew

from fire import Fire
import paho.mqtt.publish as publish
import sys
import time
from telemetrix import telemetrix
import datetime as dt
from collections import deque
import statistics
import os
import logging
import collections
import numpy as np

logging.basicConfig(
    filename=os.path.expanduser("~/telemetry.log"),
    encoding="utf-8",
    filemode="a",
    format="{message}",
    style="{",
    datefmt="", #"%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
    )
logging.addLevelName(logging.INFO, "")


# =================== GLOBAL VARIABLES ====================
cache = None
DEBUG = False

# retain_time ... also set from MAIN() .. relates number of measurements
RETAIN_TIME= 2 # average just the last n serconds
# delay_time will be set to TIMEOUT PARAMETER
DELAY_TIME = 10 # sleep between printouts/aveeragings
BOARD = None

APINS = ['0', '1' ]
APINSint = [] # this is for printout.....
CALIBS = []
FINAL_VALUES = [] # if there is calibration, provide the values and later pair TH

LAST_CALL_TIME = dt.datetime.now()

# -----------------------------------------------------------------------
# .........  FIFO CACHE
# -----------------------------------------------------------------------

class FIFOCache:
    #
    #  I allow 10 values? for 14 pins ....or.... 14 values for 10 pins?
    #
    def __init__(self, max_size=14 * 100):
        self.cache = deque(maxlen=max_size)

    def add(self, data):
        #f len(data) != 6:
        #   raise ValueError("Data must contain exactly 6 integers.")
        self.cache.append(data)

    def get_the_newest_age(self):
        current_time = dt.datetime.now()
        lowest_dt = 3600 * 24 # i am thinking about 1 minute or 15 minutes
        for data in self.cache:
            dtx = (current_time - data[3]).total_seconds()
            if dtx < lowest_dt:
                lowest_dt = dtx
        if DEBUG:
            print(f"\nD... the very last data is {lowest_dt} s. old")
        return lowest_dt


    def remove_older_than(self, retain_seconds):
        """
        AI,  [3] is time
        """
        current_time = dt.datetime.now()
        #
        seen_pins = set(data[1] for data in self.cache)
        bk_cache = deque(self.cache, maxlen=self.cache.maxlen)
        # Filter cache based on retain_seconds
        self.cache = deque(
            [data for data in self.cache if (current_time - data[3]).total_seconds() <= retain_seconds],
            maxlen=self.cache.maxlen
        )
        # Collect missing pins
        current_pins = set(data[1] for data in self.cache)
        missing_pins = seen_pins - current_pins

        # Restore at least one item for missing pins
        for data in bk_cache:
            if data[1] in missing_pins:
                self.cache.append(data)
                missing_pins.remove(data[1])
                if not missing_pins:
                    break



    def get_filtered_values(self, values):
        """
        AI,the point was - remove outliers, like decile,quartile
        """
        if len(values) < 3:
            return values
        # Calculate Q1 and Q3
        q1 = statistics.quantiles(values, n=4)[0]
        q3 = statistics.quantiles(values, n=4)[2]
        # Calculate IQR
        iqr = q3 - q1
        # Define bounds for outliers
        lower_bound = q1 - 1.5 * iqr
        upper_bound = q3 + 1.5 * iqr
        # Filter out outliers
        filtered_values = [v for v in values if lower_bound <= v <= upper_bound]
        # Calculate mean of filtered values
        return filtered_values
        #return statistics.mean(filtered_values)

    def get_avg_std(self, pin):
        values = [data[2] for data in self.cache if data[1] == pin]
        values = self.get_filtered_values(values)
        avg, std = 0, 0
        if not values:
            avg = 0
        else:
            avg = statistics.mean(values)
        if len(values) > 1:
            std = statistics.stdev(values)
        return avg, std


    def clear(self):
        self.cache.clear()

    def get_cache(self):
        return list(self.cache)

    def get_size(self):
        return len(self.cache)

    def get_size_for_pin(self, pin):
        return sum(1 for data in self.cache if data[1] == pin)



# -----------------------------------------------------------------------
# ------------------------------- ................   add stuff to chache
# -----------------------------------------------------------------------
# --------------------------------*---------------------------- callback

def the_callback(data):
    """
    get event: ANALOG ... 0 pinmode, 1 pinnumber,  2 value, 3 time
    general_record: 3 2 5 6 4
    """
    global cache
    global DEBUG, RETAIN_TIME, LAST_CALL_TIME
    NOW = dt.datetime.now()
    if (NOW - LAST_CALL_TIME).total_seconds() < 0.05:
        return
    datim = dt.datetime.fromtimestamp(data[3])
    strtime = datim.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
    LAST_CALL_TIME = NOW
    general_record = []
    DHTMODE = False
    if data[0] == 12: # DHT 12
        DHTMODE = True
        if data[1] == 0: # DHT_port and readresult==0
            general_record = [ datim, data[2], data[5], data[6], data[4]  ] # T  +++H
            #print(f'XXX Mode: {data[0]} Pin: {data[1]} Value: {data[2]} Time Stamp: {strtime}  ', end="\r")
        elif data[1] != 0: # DHT_port and error
            print("X... some DHT read error", end="\r")

    elif data[0] == 3:   #************************** JUST ANALOG ******************
        general_record = [ data[0], data[1], data[2], datim  ]

    if DEBUG:
        print(f'Pin Mode: {general_record[0]} Pin: {general_record[1]} Value: {general_record[2]} Time Stamp: {strtime}   size={cache.get_size()}', end="\r")

    cache.add( general_record )  #  push to the _queue.
    cache.remove_older_than(RETAIN_TIME)
    #cache.remove_older_than_w_filter(RETAIN_TIME)
    # time.sleep(0.020) # sleep 10ms # it

# -----------------------------------------------------------------------
#   ........ just return  NOW
# -----------------------------------------------------------------------
def now():
    return dt.datetime.now().timestamp()

# -----------------------------------------------------------------------
# ............... NOW with fractions
# -----------------------------------------------------------------------
def now_frc():
    return str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"))[:-4]


# -----------------------------------------------------------------------
# .......... string APINS  '0123' to integer list []
# -----------------------------------------------------------------------
def update_apins():
    """
    takes APINS (str) and creates integer APINSint and initializes
    """
    global APINS, APINSint, BOARD
    APINSint = []
    for i in APINS:
        print(f"i... initializing analog pin {i}:", end="")
        Ai = False
        try:
            ii = int(i)
            Ai = True
        except:
            pass

        FINAL_VALUES.append(0.0)
        if Ai:
            print(f" ANALOG at pin A{ii}")
            BOARD.set_pin_mode_analog_input(ii,differential=1,callback=the_callback)
            APINSint.append(ii)
        elif i.find("dht") >= 0:
            ii = i.replace("dht", "") # REMOVE 'dht' and pin remains!
            ii = int(ii)
            print(f" DHT  at pin /{ii}/ ... there is a trick with Humidity pin!")
            BOARD.set_pin_mode_dht(ii, the_callback, 22) # verion 22
            APINSint.append(ii)
        #ni+= 1



# -----------------------------------------------------------------------
#  .................. do calibrations....   phid...
# -----------------------------------------------------------------------
def calibrate(CALIBSi,val,err):  # OUTSOURCE
    global FINAL_VALUES

    if CALIBSi.find("phidt") == 0:
        val1 = val / 1024* 222.2 - 61.111
        err1 = err / 1024* 222.2
        # ---------------------------------------------------------
    elif CALIBSi.find("phidh") == 0:
        val1 = val / 1024* 190.6 - 40.2
        err1 = err / 1024* 190.6
        # ---------------------------------------------------------
    elif CALIBSi.find("ntc") == 0:
        # calculate R
        V5 = 5 # arduino 5V
        R1 = 100_000
        # R - I see like 100k ->75k for 30C
        Vpin = V5 * val / 1023 # BUG IN OUTPUT
        if DEBUG:
            print(f"D... Vpin=  {Vpin:_.2f} V")
        R = R1 * Vpin/( V5 - Vpin) # R1*V5/(V5-Vpin)
        R = R1 - R
        #R = R1 * (1023.0 / output - 1.0)
        #if DEBUG:
        print(f"D... R=  {R:_.2f} Ohm     adc={val:.0f}")
        A = 1.125e-3#1 / 298.15
        B = 2.347e-4#2.362e-4
        C = 0.855e-7#9.285e-8=
        # - from my previous
        A = 1.009249522e-03
        B = 2.378405444e-04
        C = 2.019292697e-07
        A =  -0.00912222479910888
        B =  0.00109640105632993
        #k1 = A + B*np.log10(R) + C*(np.log10(R))**3
        #k1 = A + B*np.log(R) + C*(np.log(R))**3
        k1 = A + B*np.log(R)
        T = 1 / k1 #- 273.15
        output = T - 273.15 # + 18.3 + 19.5# BAD TRICK
        if DEBUG:
            print(f"D.... T={T:.2f} K   {output:.2f} C ")
        # ---------------------------------------------------------
        val1 = output
        err1 = 0.0
    elif CALIBSi == "I" or CALIBSi == "1" or CALIBSi == "0" :
        val1 = val
        err1 = err
    else:
        print(f"X... {fg.red}Unknown calibration ... ", CALIBSi, fg.default)

    return val1, err1


# -----------------------------------------------------------------------
#      MAIN LOOP
# -----------------------------------------------------------------------
def analog_in():
    """
     This function establishes the pin as an
     analog input. Any changes on this pin will
     be reported through the call back function.

     :param my_board: a telemetrix instance
     """

    global cache
    global DEBUG, RETAIN_TIME, DELAY_TIME, APINSint
    global APINSint, BOARD
    # set the pin mode
    #print("i... setting callback")
    #for i in APINSint:
    #    my_board.set_pin_mode_analog_input(i, differential=1, callback=the_callback)

    # time.sleep(5)
    # my_board.disable_analog_reporting()
    # time.sleep(5)
    # my_board.enable_analog_reporting()

    print('i...done. Enter Control-C to quit.')
    loops = 0
    while True:
        loops += 1
        time.sleep(DELAY_TIME)
        cache.remove_older_than(RETAIN_TIME) #clear()
        newest_data_age = cache.get_the_newest_age()
        if newest_data_age > DELAY_TIME * 3:
            print("X... no new data for 3x timeout. Exiting.  sec=", newest_data_age)
            sys.exit(1)
        if cache.get_size() == 0:
            print("X... possible loss of sensor, sleeping 5 sec.")
            time.sleep(5)
            try:
                BOARD = telemetrix.Telemetrix()
                update_apins()
            except:
                pass
            print("X... possible loss of sensor, second try to connect after 5sec")
            time.sleep(5)
            try:
                BOARD = telemetrix.Telemetrix()
                update_apins()
            except:
                pass
        # ------ form a printout line
        if DEBUG: print()
        LINEOUT = ""
        #  -----------   construct LINEOUT, LOG and PUBLISH -------------------HERE--
        for i in APINSint:
            # ********************
            val, err = cache.get_avg_std(i)
            # *******************
            FINAL_VALUES[i] = None
            # ------decide on calibrations
            if len(CALIBS) >= i and CALIBS[i] is not None:
                val,err = calibrate(CALIBS[i],val,err)  # OUTSOURCE
                FINAL_VALUES[i] = val
            # PUSBLISH MOSQUITO - just one value at a time, why not 2+dew?
            if CALIBS[i] is None:
                topic = f"tele/pin{i}"
            else:
                topic = f"tele/{CALIBS[i]}{i}"
            if 0:#DHT: #*************************************override
                topic = f"tele/dht_t{i}"
            if FINAL_VALUES[i] is not None:
                publish.single( topic, f"{FINAL_VALUES[i]:.1f}", hostname="localhost")

            LINEOUT = f"{LINEOUT} P{i}: {val:5.1f} +- {err:5.1f} "
            if DEBUG:
                #print(f" {cache.get_size()} / {cache.get_size_for_pin(0)}" )
                #print()
                print(f" cached pin{i} -  pin/allpins : {cache.get_size_for_pin(i)} /  {cache.get_size()} {LINEOUT}" )

        logging.info(LINEOUT)
        print(f"{now_frc()} {LINEOUT}", end="\r")
        if loops % 10 == 0:print()
        # Find_TH_Pairs() FINAL_VALUES


# -----------------------------------------------------------------------
#   ...  deal with arguments
# -----------------------------------------------------------------------
def breakout2list(setup):
    """
    split the commandline argument 0,1,2 etc... all possibilities
    """
    RES = [None]
    if type(setup) == tuple: # 0,1,2
        RES = list(setup)
        RES = [str(i) for i in RES]
    if type(setup) == str: # 0,1dh,2
        if setup.find(",") > 0:
            RES = setup.split(",")
        elif setup.find(",") < 0:
            RES = [setup]
        else:
            print("X... bad list in argument")

    if type(setup) == int: # 0
        RES = [setup]

    if setup is None:
        RES = [None]
    if type(setup) is bool:
        RES = [None]

    print("X /list/str/ => ", type(RES), type(RES[0]), RES)
    return RES


# ----------------------------------------------------------------------
# .........   MAIN
# --------------------------------- MAIN PART ----------------
def main( setup, calibrations=None, serial_port=None, timeout=30, retain_time=2, debug =False):
    """
    if DHT...set conscutive pins ...
    organize_in(board, APINS, DHT) ...
    ...  there ...  set_pin_mode_analog_input or DHT
    ...  and then ... while loop///
   .,.. retain - retain_time keep all values from the last r seconds
    """
    global BOARD
    global cache
    global APINS, APINSint
    global CALIBS
    global DELAY_TIME, RETAIN_TIME
    global DEBUG

    print('CHECK WITH MQTT:      mosquitto_sub -t "#" -v  ')
    if debug:DEBUG = True

    DELAY_TIME = timeout
    RETAIN_TIME = retain_time

    APINS = breakout2list(setup)
    if calibrations is None:
        CALIBS = [None for i in APINS]
        print("H...  calibrations can be as")
        print("H...    -c phidt,phidh,I")
        print("H...    -c ...")
    else:
        CALIBS =breakout2list(calibrations)

        if len(APINS) != len(CALIBS):
            print("X... uneven calibrations list, clearing calibrations")
            CALIBS = [None for i in APINS]
    print(f"i... calibrations: {CALIBS}")

    print("i... allocationg cache ...")
    cache = FIFOCache()
    print("i... allocationg board ... ")
    BOARD = telemetrix.Telemetrix()
    update_apins()
    try:
        analog_in()
    except KeyboardInterrupt:
        BOARD.shutdown()
        sys.exit(0)
    return


# -----------------------------------------------------------------------
#
# -----------------------------------------------------------------------

if __name__ == "__main__":
    Fire( main )
