#!/usr/bin/env python3
#
# see
#python3 -m serial.tools.list_ports
#
"""
10/2024 * CALIBRATIONS MUST BE HERE.....
____________________________________________________________

yellow - mqtt
green - named pipe


  works with arduino uno  - /dev/ttyACM0 (analog pins 01)
       arduino-cli compile -b arduino:avr:uno -v
       arduino-cli upload -b arduino:avr:uno -p /dev/ttyACM0

  works with arduino nano (DHT22 sensor on pin2)
       arduino-cli compile -b arduino:avr:nano:cpu=atmega328old
       arduino-cli upload -b arduino:avr:nano:cpu=atmega328old -p /dev/ttyUSB0

?? arduino-cli upload -b arduino:avr:mega -p /dev/ttyACM0
#
###################################
#


  arduino-cli compile -b arduino:avr:uno -v
  arduino-cli  board list
  arduino-cli  version
  arduino-cli  core list
  arduino-cli  core install arduino:avr
  arduino-cli  core list
  arduino-cli compile -b arduino:avr:uno -v
  arduino-cli lib install Telemetrix4Arduino
  arduino-cli compile -b arduino:avr:uno -v
  arduino-cli lib install Servo
  arduino-cli compile -b arduino:avr:uno -v
  arduino-cli upload  -b arduino:avr:uno -p /dev/ttyACM0
  ./telemana.py 01 -t 5
  arduino-cli  board list
  arduino-cli core update-index
  arduino-cli core search arduino
  arduino-cli compile -b arduino:avr:diecimila:cpu=atmega328 -v
  arduino-cli upload  -b arduino:avr:diecimila:cpu=atmega328 -p /dev/ttyUSB0
  ./telemana.py 01 -t 5

https://mryslab.github.io/telemetrix/
https://mryslab.github.io/telemetrix/debug/
"""
from fire import Fire
import sys
import time

from telemetrix import telemetrix
import datetime as dt
import statistics
import os
import paho.mqtt.publish as publish

import numpy as np
from console import fg, bg

import logging
logging.basicConfig(
    filename=os.path.expanduser("~/telemetry.log"),
    encoding="utf-8",
    filemode="a",
    format="{message}",
    style="{",
    datefmt="", #"%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
    )
logging.addLevelName(logging.INFO, "")
"""
Setup pins for analog input and monitor its changes
"""

# Setup a pin for analog input and monitor its changes
DELAY_SEC = 30
REMEMBER_LAST_SEC = 2.0 # inside cache queue


APINS = ['0', '1']
APINSint = None # this is for printout.....
CALIBS = None # phidt phidh  .....

DEBUG = False

LASTPIN = 12 # just some high number APINS[-1] -- Sets C A C H E limit!!!????
cache = None
baord = None # Trying to allow soft restart
# Callback data indices FOR ANALOG!!!!
CB_PIN_MODE = 0
CB_PIN = 1
CB_VALUE = 2
CB_TIME = 3

# indices into callback data for valid data DHT
# REPORT_TYPE = 0
# READ_RESULT = 1
# PIN = 2
# DHT_TYPE = 3
# HUMIDITY = 4
# TEMPERATURE = 5
# TIME = 6

# indices into callback data for error report DHT
# REPORT_TYPE = 0
# READ_RESULT = 1
# PIN = 2
# DHT_TYPE = 3
# TIME = 4

PRONCE_TOPIC = []
def printonce(topic):
    global PRONCE_TOPIC
    if not topic in PRONCE_TOPIC:
        print(f'D... You can use in cfg.json:  mminput1_cfg": "dial xxx;0;100;60;{topic}"')
        PRONCE_TOPIC.append(topic)


def now():
    return dt.datetime.now().timestamp()

def now_frc():
    return str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"))[:-4]

def write2pipe(pipe_path, WHAT):
    if os.path.exists(pipe_path):
        try:
            fd = os.open(pipe_path, os.O_WRONLY | os.O_NONBLOCK) # non blocking
            os.write(fd, WHAT.encode()  )
            os.write(fd, b"\n")
            os.close(fd)
        except OSError:
            print("Pipe is not open for writing.")
    else:
        print("Pipe does not exist.")



class Telemetrix_Input_Cache:
    #
    # contains All, read<=Just Values...
    #
    # value,age
    #
    def __init__(self):
        global REMEMBER_LAST_SEC
        self._cache = dict()
        self._freq = dict()
        self._decay = REMEMBER_LAST_SEC # forget data after [s]
        self._queue = {}
        for i in range(LASTPIN + 1): # queue has 12 elements
            self._queue[i] = []

    def now(self):
        return dt.datetime.now().timestamp()

    def push(self, data):
        """
        force the cache-pin value to the queue; record NOW, remove  olds (1st time here)
        """
        now = self.now()
        #print(f"D...  push    {data}  ")
        pin_number = data[CB_PIN]
        #print(f"D.... push pin numner=={pin_number}  {CB_PIN}==1")
        self._queue[pin_number].append( data ) # all 4
        while (now - self._queue[pin_number][0][CB_TIME]) > self._decay:
            self._queue[pin_number].pop(0)
            #print(f" pin:{pin_number}  drop")
        #print(f" pin:{pin_number} {len(self._queue[pin_number])} ")


    def get_avg(self, pin_number, treatH=False):
        """
        get average, also acase of Humidity DHT
        """
        now = self.now()

        if  pin_number in self._queue:
            # also here remove old values (what if no callback called)
            try:
                while (now - self._queue[pin_number][0][CB_TIME]) > self._decay:
                    self._queue[pin_number].pop(0)
            except:
                pass
            vals = [row[2] for row in self._queue[pin_number]]
            if treatH: # 4 here
                vals = [row[4] for row in self._queue[pin_number]]
            avg = sig = 0
            if len(vals) > 0:
                avg = statistics.mean(vals)
            if len(vals) > 1:
                sig = statistics.stdev(vals)
            #
            return avg, sig
        else:
            return 0, 0

    def get_nq(self):
        """
        get points in queue
        """
        maxlen = 0
        for i in range(LASTPIN + 1): # queue has 12 elements
            if len(self._queue[i]) > maxlen:
                maxlen = len(self._queue[i])
        return maxlen


def calculate_dew_point(t=15, h=70):
    a = 17.27
    b = 237.7
    alpha = ((a * t) / (b + t)) + math.log(h / 100.0)
    dew_point = (b * alpha) / (a - alpha)
    return dew_point




def my_callbk(data):
    """
    A callback function to report data changes.
    This will print the pin number, its reported value and
    the date and time when the change occurred

    :param data: [pin, current reported value, pin_mode, timestamp]
    """
    global cache
    global DEBUG
    if DEBUG:
        #???? Pin Mode: {data[CB_PIN_MODE]} Pin: {data[CB_PIN]} Value: {data[CB_VALUE]}
        if data[CB_PIN_MODE] == 3:
            print(f'Pin Mode: {data[CB_PIN_MODE]} Pin: {data[CB_PIN]} Value: {data[CB_VALUE]} ///   {data}  /// ')
        else:
            print(f'PinMODE - Error - PIN# - ValueH - ValueT - Time ///   {data}  /// ')

    # PINMODE is like 2,3,  for analog.      but DHT it is REPORT_TYPE==12
    newdata = []
    #
    # DHT :  12,0,2,22,H,T,time **************************************
    #
    if data[0] == 12 and data[1] == 0: # DHT_port and readresult==0
        #print("D... DHTMODE", data)
        # pinmode pin value time
        # dhttype3 pin2 temp5  time6
        newdata = [ data[3], data[2], data[5], data[6], data[4]  ] # T  +++H
        cache.push( newdata )  #  push to the _queue.
    elif data[0] == 12 and data[1] != 0: # DHT_port and error
        print("X... some DHT read error", end="\r")
    # ******************************** JUST ANALOG ******************
    else:# normally ANALOG
        #print("i... ANAMODE")
        cache.push( data )  #  push to the _queue.
    time.sleep(0.1) # LIMIT TO 10Hz


# ****************************************************************
#
#
#
# ****************************************************************
def last_init(my_board ):
    global cache
    global APINS
    global APINSint
    # set the pin mode
    #my_board.set_pin_mode_digital_input(pin1, callback=my_callbk)
    ni = 0
    APINSint = [] # REDEFINE
    for i in APINS:
        print(f"i... initializing {i}:", end="")
        Ai = False
        try:
            ii = int(i)
            Ai = True
        except:
            pass

        if Ai:
            print(f" ANALOG at pin A{ii}")
            my_board.set_pin_mode_analog_input( ii,differential=1,callback=my_callbk)
            APINSint.append(ii)
        elif i.find("dht") >= 0:
            ii = i.replace("dht", "")
            ii = int(ii)
            print(f" DHT  at pin /{ii}/ ... there is a trick with Humidity pin!")
            my_board.set_pin_mode_dht(ii, my_callbk, 22) # verion 22
            APINSint.append(ii)

        ni+= 1

    print('Enter Control-C to quit.')
    # my_board.enable_digital_reporting(12)



def organize_in(my_board, serial_port):
    """
     This function establishes the pin as a
     digital input. Any changes on this pin will
     be reported through the call back function.

     :param my_board: a pymata4 instance
     :param pin: Arduino pin number
    """
    global cache
    global board
    global CALIBS
    last_init( my_board)

    try:
        last = dt.datetime.now()
        print("Pins:", APINS)
        print("Ints:", APINSint)
        print('CHECK MQTT:      mosquitto_sub -t "#" -v  ')
        while True:
            # protection against PC sleep
            #print("-----------------")
            #print(dt.datetime.now(),  last)
            #print("D... real delay",( dt.datetime.now() - last).total_seconds())
            #print("D... predefined delay", DELAY_SEC + 1 * len(apins))

            # Tryin to softly restart
            if (dt.datetime.now() - last).total_seconds() > (DELAY_SEC + 1 * len(APINS)):
                board = telemetrix.Telemetrix( com_port=serial_port )
                # Try to crash!!!!
                my_board = board
                last_init( my_board )

            last = dt.datetime.now()
            time.sleep(DELAY_SEC)
            iport = 1
            cumstr = f"{cache.get_nq():03d} {now_frc()},"
            cali = -1

            inx = -1
            for i in APINSint:
                inx += 1
                DHT = False
                if APINS[inx].find("dht") >= 0: DHT = True

                cali += 1 # catch up with CALIBS

                output, dout= cache.get_avg(i)

                if len(CALIBS) >= cali and CALIBS[cali] is not None  : #----when some calibration=>Calibrate
                    # ---------------------------------------------------------
                    if CALIBS[cali] == "phidt":
                        output = output / 1024* 222.2 - 61.111
                    # ---------------------------------------------------------
                    elif CALIBS[cali] == "phidh":
                        output = output / 1024* 190.6 - 40.2
                    # ---------------------------------------------------------
                    elif CALIBS[cali] == "ntc":
                        # calculate R
                        V5 = 5 # arduino 5V
                        R1 = 100_000
                        # R - I see like 100k ->75k for 30C
                        Vpin = V5 * output / 1023
                        if DEBUG:
                            print(f"D... Vpin=  {Vpin:_.2f} V")
                        R = R1 * Vpin/( V5 - Vpin) # R1*V5/(V5-Vpin)
                        R = R1 - R
                        #R = R1 * (1023.0 / output - 1.0)
                        #if DEBUG:
                        print(f"D... R=  {R:_.2f} Ohm     adc={output:.0f}")
                        A = 1.125e-3#1 / 298.15
                        B = 2.347e-4#2.362e-4
                        C = 0.855e-7#9.285e-8=
                        # - from my previous
                        A = 1.009249522e-03
                        B = 2.378405444e-04
                        C = 2.019292697e-07

                        A =  -0.00912222479910888
                        B =  0.00109640105632993

                        #k1 = A + B*np.log10(R) + C*(np.log10(R))**3
                        #k1 = A + B*np.log(R) + C*(np.log(R))**3
                        k1 = A + B*np.log(R)
                        T = 1 / k1 #- 273.15
                        output = T - 273.15 # + 18.3 + 19.5# BAD TRICK
                        if DEBUG:
                            print(f"D.... T={T:.2f} K   {output:.2f} C ")
                    # ---------------------------------------------------------
                    elif CALIBS[cali] == "0":
                        output = output
                    # ---------------------------------------------------------
                    else:
                        print(f"X... {fg.red}Unknown calibration ... ", CALIBS[cali], fg.default)

                # ---- add to printout ------------------------------------
                cumstr = f"{cumstr} PIN{i}: {output:5.1f}+-{dout:.1f}  "#, {cache.get_avg(i)[1]:5.1f}, "

                # go for mmap
                if CALIBS[cali] is None:
                    topic = f"tele/pin{i}"
                else:
                    topic = f"tele/{CALIBS[cali]}{i}"
                if DHT: #*************************************override
                    topic = f"tele/dht_t{i}"

                printonce(topic)

                output = str(output)
                publish.single( topic, output, hostname="localhost") # PUBLISH; I see the whole takes ~4-5ms

                if DHT: #*************************************
                    topic2 = f"tele/dht_h{i}"
                    output2 = cache.get_avg(i, treatH=True)[0]
                    #output2 = str(output2)
                    publish.single( topic2, output2, hostname="localhost") # PUBLISH; I see the whole takes ~4-5ms
                    cumstr = f"{cumstr} dhtH{i}: {output2:5.1f}  "#, {cache.get_avg(i)[1]:5.1f}, "

                    printonce(topic2)
                    topic3 = f"tele/dht_dw{i}"
                    output3 = calculate_dew_point(output, output2)
                    publish.single( topic3, output3, hostname="localhost") # PUBLISH; I see the whole takes ~4-5ms
                    cumstr = f"{cumstr} dhtDW{i}: {output3:5.1f}  "#, {cache.get_avg(i)[1]:5.1f}, "

                #print(f"{topic}   {output}")
                #print(f"{topic2}   {output2}")
                #
                #write2pipe( f"/tmp/flashcam_fifo_800{iport}", output  )
                #write2pipe( f"/tmp/flashcam_fifo_800{iport}", str(cache.read(i)[0])  )
                iport += 1
                time.sleep(0.35)

            cumstr = f"{cumstr}"
            logging.info(cumstr)
            print(f"{cumstr}")

    except KeyboardInterrupt:
        my_board.shutdown()
        sys.exit(0)

# *************************************************************************
# *************************************************************************
# 01 .....  analog pins 0 1
# 01234 ..  analog0-4
# DHT22 2 ...   ?
#
#    IF DHT:
#          com_port = "/dev/ttyUSB0"
#          ### dht(board, 2, the_callback, 22)
#          my_board.set_pin_mode_dht(2, the_callback, 22)
#
#./telemana.py 2 -c /dev/ttyUSB0 -D
#
# pinmode 12
# pin#  0
# value 2
# time 22
# hum
# temp
# time
# *************************************************************************
# *************************************************************************

def breakout2list(setup):
    """
    split the commandline argument 0,1,2 etc... all possibilities
    """
    RES = [None]
    if type(setup) == tuple: # 0,1,2
        RES = list(setup)
        RES = [str(i) for i in RES]
    if type(setup) == str: # 0,1dh,2
        if setup.find(",") > 0:
            RES = setup.split(",")
        elif setup.find(",") < 0:
            RES = [setup]
        else:
            print("X... bad list in argument")

    if type(setup) == int: # 0
        RES = [setup]

    if setup is None:
        RES = [None]
    if type(setup) is bool:
        RES = [None]

    print("X /list/str/ => ", type(RES), type(RES[0]), RES)
    return RES

# /////////////////////////////////////////////////////////////////////////////////////////////
# /////////////////////////////////////////////////////////////////////////////////////////////
# /////////////////////////////////////////////////////////////////////////////////////////////
# /////////////////////////////////////////////////////////////////////////////////////////////
# /////////////////////////////////////////////////////////////////////////////////////////////
def main( setup, calibrations=None, serial_port=None, timeout=30, debug =False):
    """
    if DHT...set conscutive pins ...
    organize_in(board, APINS, DHT) ...
    ...  there ...  set_pin_mode_analog_input or DHT
    ...  and then ... while loop///
    """
    global board # Trying to soft restart
    global cache
    global APINS
    global CALIBS
    global LASTPIN
    global DELAY_SEC
    global DEBUG

    if debug:DEBUG = True

    DELAY_SEC = timeout

    APINS = breakout2list(setup)
    if calibrations is None:
        CALIBS = [None for i in APINS]
    else:
        CALIBS =breakout2list(calibrations)

        if len(APINS) != len(CALIBS):
            print("X... uneven calibrations list")
            CALIBS = [None for i in APINS]
    print(f"i... calibrations: {CALIBS}")


    cache = Telemetrix_Input_Cache() # my values store = PUT TO GLOBAL
    #
    #   THIS IS BOARD INITITALIZATION
    #
    board = telemetrix.Telemetrix( com_port=serial_port )

    try:
#        if DHT:
#            APINS = APINS + [APINS[0] + 1]
        organize_in(board,  serial_port)
    except KeyboardInterrupt:
        board.shutdown()
        sys.exit(0)
        return

if __name__ == "__main__":
    Fire( main )
