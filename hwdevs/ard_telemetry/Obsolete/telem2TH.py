#!/usr/bin/env python3
import sys
import time

from telemetrix import telemetrix
import datetime as dt
import statistics


"""
Monitor a digital input pin
"""

"""
Setup a pin for digital input and monitor its changes
"""

# Setup a pin for analog input and monitor its changes
LASTPIN = 1
DIGITAL_PIN = 12  # arduino pin number
ANALOG_PIN0 = 0 #2  # arduino pin number (A2)
ANALOG_PIN1 = 1 #
ANALOG_PIN4 = 4 #2  # arduino pin number (A2)
DELAY_SEC = 15 #
# Callback data indices
CB_PIN_MODE = 0
CB_PIN = 1
CB_VALUE = 2
CB_TIME = 3

def now():
    return dt.datetime.now().timestamp()


def write2pipe(pipe_path, WHAT):
    if os.path.exists(pipe_path):
        try:
            fd = os.open(pipe_path, os.O_WRONLY | os.O_NONBLOCK) # non blocking
            os.write(fd, WHAT.encode()  )
            os.write(fd, b"\n")
            os.close(fd)
        except OSError:
            print("Pipe is not open for writing.")
    else:
        print("Pipe does not exist.")




class Telemetrix_Input_Cache:
    #
    # contains All, read<=Just Values...
    #
    # value,age
    #
    def __init__(self):
        self._cache = dict()
        self._freq = dict()
        self._decay = 1.0 # forget data after [s]
        self._queue = {}
        for i in range(LASTPIN + 1):
            self._queue[i] = []

    def now(self):
        return dt.datetime.now().timestamp()

    def push(self, data):
        now = self.now()
        pin_number = data[CB_PIN]
        self._queue[pin_number].append( data ) # all 4
        while (now - self._queue[pin_number][0][CB_TIME]) > self._decay:
            self._queue[pin_number].pop(0)
            #print(f" pin:{pin_number}  drop")
        #print(f" pin:{pin_number} {len(self._queue[pin_number])} ")


    def get_avg(self, pin_number):
        if  pin_number in self._queue:
            qlen = len(self._queue[pin_number])
            vals = [row[2] for row in self._queue[pin_number]]
            avg = statistics.mean(vals)
            sig = statistics.stdev(vals)
            #
            return avg, sig
        else:
            return 0

    def write(self, data ): # (4 - pintype , pinnum ,   value , stamp)
        pin_number = data[CB_PIN]
        self._cache[pin_number] = data
        # DEBUG
        # if not pin_number in self._freq:
        #     self._freq[pin_number] = 1
        # else:
        #     self._freq[pin_number]+=1

    def read(self, pin_number):
        if pin_number in self._cache:
            #print( pin_number, " - ", self._cache[pin_number])
            age = round(self.now() - self._cache[pin_number][CB_TIME], 2)
            qlen = 0
            if  pin_number in self._queue:
                qlen = len(self._queue[pin_number])
            return ( self._cache[pin_number][CB_VALUE],
                     age,
                     qlen
                     )
        else:
            return (None, None, None)
            #raise Exception(f'Have not seen an update from pin {pin_number}')

    def temp(self, pin_number):
        if pin_number in self._cache:
            return  round( self._cache[pin_number][CB_VALUE] / 1024* 222.2 - 61.111, 1)

    def humi(self, pin_number):
        if pin_number in self._cache:
            return  round( self._cache[pin_number][CB_VALUE] / 1024* 190.6 - 40.2, 1)

    def read_freq(self, pin_number):
        try:
            return self._freq[pin_number]
        except KeyError:
            raise Exception(f'Have not seen an update from pin {pin_number}')



cache = Telemetrix_Input_Cache()
#cache.write( [ANALOG_PIN0, 0, 0, 0] ) # Mode,Numb,Val,Time

def the_callback(data):
    """
    A callback function to report data changes.
    This will print the pin number, its reported value and
    the date and time when the change occurred

    :param data: [pin, current reported value, pin_mode, timestamp]
    """
    #date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data[CB_TIME]))
    #print(f"{date} ", end="")
    #now = dt.datetime.now().timestamp()
    #etime = data[CB_TIME]
    #print(f'Pin Mode: {data[CB_PIN_MODE]} Pin: {data[CB_PIN]} Value: {data[CB_VALUE]}  ')
    cache.write(  data )
    cache.push(data)


def digital_in(my_board, pin1, apins):
    """
     This function establishes the pin as a
     digital input. Any changes on this pin will
     be reported through the call back function.

     :param my_board: a pymata4 instance
     :param pin: Arduino pin number
     """

    # set the pin mode
    # my_board.set_pin_mode_digital_input(pin1, callback=the_callback)
    ni = 0
    for i in apins:
        if ni > LASTPIN: break
        print("D... analog pin UP ", i)
        my_board.set_pin_mode_analog_input(i, differential=1, callback=the_callback)
        ni+= 1

    print('Enter Control-C to quit.')
    # my_board.enable_digital_reporting(12)
    try:
        while True:
            #dd = abs(round(  (cache.read(4)[0] - cache.get_avg(4)[0]) /cache.get_avg(4)[1] , 1))
            print( dt.datetime.now() , end=" ")
            for i in apins:
                print( f"PIN{i}: {cache.temp(i)}   ", end="")
            print("")
            write2pipe( "/tmp/flashcam_fifo_8001", str(cache.temp(0)) )
            time.sleep(0.4)
            write2pipe( "/tmp/flashcam_fifo_8002", str(cache.humi(1)) )
            time.sleep(0.4)
            # with open("/tmp/flashcam_fifo_8003", "a") as f:
            #     f.write(f"{cache.temp(2)}\n")
            #     time.sleep(0.2)
            # with open("/tmp/flashcam_fifo_8004", "a") as f:
            #     f.write(f"{cache.temp(3)}\n")
            #     time.sleep(0.2)
            time.sleep(DELAY_SEC)

    except KeyboardInterrupt:
        board.shutdown()
        sys.exit(0)


board = telemetrix.Telemetrix()


try:
    digital_in(board, DIGITAL_PIN, [ 0, 1] )
except KeyboardInterrupt:
    board.shutdown()
    sys.exit(0)
