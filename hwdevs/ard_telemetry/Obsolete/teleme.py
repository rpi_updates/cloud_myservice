#!/usr/bin/env python3
import sys
import time
import datetime as dt
from telemetrix import telemetrix

import logging
# logging.basicConfig(format="%(levelname)s:%(name)s:%(message)s")
logging.basicConfig(
    filename="telemetry.log",
    encoding="utf-8",
    filemode="a",
    format="{asctime} , {levelname} , {message}",
    style="{",
    datefmt="%Y-%m-%d %H:%M",
    level=logging.INFO,
    )
logging.addLevelName(logging.INFO, "TRHvalues")
"""
Monitor a digital input pin
"""

"""
Setup a pin for digital input and monitor its changes
"""

# # Setup a pin for analog input and monitor its changes
# DIGITAL_PIN = 12  # arduino pin number
# # Callback data indices
# CB_PIN_MODE = 0
# CB_PIN = 1
# CB_VALUE = 2
# CB_TIME = 3


# Set up a pin for analog input and monitor its changes
ANALOG_PINt = 0  # arduino pin number (A2)
ANALOG_PINh = 1  # arduino pin number (A2)
# Callback data indices
CB_PIN_MODE = 0
CB_PIN = 1
CB_VALUE = 2
CB_TIME = 3


values = { 0:[], 1:[],
           'avg0':0., 'avg1':0. ,
           'f0':'/tmp/flashcam_fifo_8001',
           'f1':'/tmp/flashcam_fifo_8002'
          }


def the_callback(data):
    """
    A callback function to report data changes.
    This will print the pin number, its reported value and
    the date and time when the change occurred

    :param data: [pin, current reported value, pin_mode, timestamp]
    """
    global values
    #date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data[CB_TIME]))
    #print(f"{date}: ", end="")
    if data[CB_PIN]==0:
        cal = 222.2*data[CB_VALUE]/1024-61.111
        values[ data[CB_PIN] ].append( cal )
        print("t", end="", flush=True)
        if len(values[0])>0:
            values['avg0'] = round(sum(values[0]) / len(values[0]),1)
        #print(f"Pin Mode: {data[CB_PIN_MODE]} Pin: {data[CB_PIN]} Value: {data[CB_VALUE]}  T={cal:.1f} ", end="")

    if data[CB_PIN]==1:
        cal = 190.6*data[CB_VALUE]/1024-40.2
        values[ data[CB_PIN] ].append( cal )
        print("h", end="", flush=True)
        if len(values[1])>0:
            values['avg1'] = round(sum(values[1]) / len(values[1]),1)
        #print(f"Pin Mode: {data[CB_PIN_MODE]} Pin: {data[CB_PIN]} Value: {data[CB_VALUE]}           RH={cal:.1f} ", end='')

    #print( f"  dat = {len(values[ data[CB_PIN] ])}")



def analog_in(my_board, pint, pinh):
    """
     This function establishes the pin as an
     analog input. Any changes on this pin will
     be reported through the call back function.

     :param my_board: a telemetrix instance
     :param pin: Arduino pin number
     """
    global values
    # set the pin mode
    my_board.set_pin_mode_analog_input(pint, differential=1, callback=the_callback)
    my_board.set_pin_mode_analog_input(pinh, differential=1, callback=the_callback)

    time.sleep(3)
    if len(values[0])>0:
        #values['avg0'] = round(sum(values[0]) / len(values[0]),1)
        with open(values['f0'],"w") as f:
            f.write(str(values['avg0']))
            f.write("\n")
    time.sleep(0.2)
    print("i... avg temp is over, going to humidity")
    if len(values[1])>0:
        print("D... opening ", values['f1'])
        #values['avg1'] = round( sum(values[1]) / len(values[1]),1 )
        with open(values['f1'],"w") as f:
            print("D... opened and ...", flush=True)
            print("writing",values['avg1'])
            print("D... witing EOL")
            f.write(str(values['avg1']))
            f.write("\n")
    print("i... avg RH is over, going to shutdown")

    print(f"@ {str(dt.datetime.now())[:-5]} T = {values['avg0']}   RH = {values['avg1']}   ")
    logging.info(f"{values['avg0']}, {values['avg1']}")
    print("i... pipes written, shutting down")
    time.sleep(4)
    board.shutdown()
    sys.exit(0)


    # # my_board.disable_analog_reporting(pinh)
    # print('Enter Control-C to quit.')
    # try:
    #     while True:
    #         time.sleep(1) # measure
    #         my_board.disable_analog_reporting(pint)
    #         my_board.disable_analog_reporting(pinh)
    #         if len(values[0])>0:
    #             values['avg0'] = round(sum(values[0]) / len(values[0]),1)
    #         if len(values[1])>0:
    #             values['avg1'] = round( sum(values[1]) / len(values[1]),1 )

    #         print(f"@ {str(dt.datetime.now())[:-5]} T = {values['avg0']}   RH = {values['avg1']}   ")
    #         logging.info(f"{values['avg0']}, {values['avg1']}")
    #         with open(values['f0'],"w") as f:
    #             f.write(str(values['avg0']))
    #             f.write("\n")
    #         with open(values['f1'],"w") as f:
    #             f.write(str(values['avg1']))
    #             f.write("\n")
    #         print("i... pipes written")

    #         #print("sleep 5 ")
    #         time.sleep(14) # dont measure
    #         values[pint]*=0
    #         values[pinh]*=0
    #         my_board.enable_analog_reporting(pint)
    #         my_board.enable_analog_reporting(pinh)
    # except KeyboardInterrupt:
    #     board.shutdown()
    #     sys.exit(0)


def digital_in(my_board, pin):
    """
     This function establishes the pin as a
     digital input. Any changes on this pin will
     be reported through the call back function.

     :param my_board: a pymata4 instance
     :param pin: Arduino pin number
     """

    # set the pin mode
    my_board.set_pin_mode_digital_input(pin, callback=the_callback)

    print('Enter Control-C to quit.')
    # my_board.enable_digital_reporting(12)
    try:
        while True:
            time.sleep(.0001)
    except KeyboardInterrupt:
        board.shutdown()
        sys.exit(0)


# =========================================================
# =========================================================
#
#   MAIN
#
# =========================================================
# =========================================================

board = telemetrix.Telemetrix()

#try:
#digital_in(board, DIGITAL_PIN)
analog_in(board, ANALOG_PINt,  ANALOG_PINh)
#except KeyboardInterrupt:
time.sleep(8)
board.shutdown()
sys.exit(0)
