#!/usr/bin/env python3
#myservice_description: LIB:relay+sound

"""
play a sound and relay switch
on rasp. to use mpv:
sudo apt install python3-gst-1.0

apt install usbrelay
  sudo usermod -a -G plugdev $USER


"""

from fire import Fire
#import RPi.GPIO as GPIO
import time
import random
# from playsound import playsound # NOT ON RPI
import subprocess as sp
import os

import socket
import subprocess
import shlex


rl14 = [19, 13, 6, 5] # raspberry channels
rlusb = [ "959BI_1" ] # usbrelay name

def set_relay_old( onoff, channel ):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(rl14[channel], GPIO.OUT)
    if onoff == 1:
        GPIO.output(rl14[channel], 1 )
    else:
        GPIO.output(rl14[0], 0)

def set_relay( onoff, channel=0 ):
    if channel != 0:
        print("X.... bad channel")
        return
    CMD = f"usbrelay {rlusb[0]}={onoff}"
    print(CMD)
    CMD = shlex.split(CMD)
    print(CMD)
    sp.check_call( CMD )

def get_dir():
    ful = os.path.realpath(__file__)
    return os.path.dirname(ful)



def buzz():
    CMD = f"/usr/bin/mpv {get_dir()}/buzzer.mp3"
    print(CMD)
    sp.check_call( CMD.split() )

def cheer():
    CMD = f"/usr/bin/mpv {get_dir()}/cheer.mp3"
    print(CMD)
    sp.check_call( CMD.split() )


def r1_on( tim=1):
    set_relay(1,channel = 0 )
    print("i... playing buzz")
    #playsound( "buzzer.mp3" )
    buzz()
    print("i... sleeping")
    time.sleep( tim )
    print("i.. DONE")
    set_relay(0,channel = 0 )


def r2_on( tim=1):
    set_relay(1,channel = 1 )
    print("i... playing")
    #playsound( "buzzer.mp3" )
    cheer()
    print("i... sleeping")
    time.sleep( tim )
    print("i.. DONE")
    set_relay(0,channel = 1 )



def null():
    for i in range(4):
        set_relay(0,channel = i )


def alli():
    for i in range(4):
        set_relay(1,channel = i )


def main():
    for i in range(4):
        set_relay( random.randrange(2) ,channel = i )


def server():
    print("i.. listen port 12345")
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', 12345))  # Replace with appropriate host and port
    server_socket.listen(5)
    print("i... ininite loop:")
    #try:
    while True:
        client_socket, addr = server_socket.accept()
        command = client_socket.recv(1024).decode('utf-8')
        print("i... received", command)
        if command.find('buzz')>=0:
            print("r1on")
            #subprocess.run(['/path/to/buzz'])
            r1_on()
        elif command.find('cheer')>=0:
            print("r2on")
            #subprocess.run(['/path/to/cheer'])
            r2_on()
        client_socket.close()
    #except KeyboardInterrupt:
    server_socket.close()


if __name__=="__main__":
    Fire( {'main':main,
           'r1_on':r1_on,
           'r2_on':r2_on,
           'buzz':buzz,
           'cheer':cheer,
           'null':null,
           'all':alli,
           'server':server,
           }
         )
#  pulseaudio --start
# pulseaudio --check
#  pactl list
#    sudo apt install pulseaudio-utils
#    sudo apt install pulseaudio
