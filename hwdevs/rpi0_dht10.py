#!/usr/bin/env python3
#-----------------------------------------
# https://pypi.org/project/Adafruit-DHT/
#  - pip3 install Adafruit_DHT
#  - https://github.com/adafruit/Adafruit_Python_DHT/
# raspi-config
#  - Interface/1-wire ON
# CONNECTION:
#  - https://cdn-shop.adafruit.com/datasheets/Digital+humidity+and+temperature+sensor+AM2302.pdf
#  - AM2302  digital signal via 1-wire bus
#  -  red(+)*VDD   yellow(data) black(-)*GND
#  -  yellow(data)-1kOhm-VDD
# On RPI0
#  - https://pinout.xyz/
#  - VDD 3.3V p17; GND p20; data p19 (GPIO10)
#---------- it seems that it works with white dht22/am2302 ; left VDD;2.Data;3 nil;4 GND
#----------------------------------------
import Adafruit_DHT
# Sensor type
# Possible entries:
# Adafruit_DHT .DHT11, Adafruit_DHT .DHT22, Adafruit_DHT .AM2302
#sensor = Adafruit_DHT.DHT22
sensor = Adafruit_DHT.AM2302
# 1 - Wire - Pin. Use BCM designation!
pin = 10
hum, temp = Adafruit_DHT.read (sensor, pin)
if hum is not None and temp is not None:
    print( f"Temp: {temp:.1f}  Humidity: {hum:.1f} ")
else:
    print("Read error. Try again!")
