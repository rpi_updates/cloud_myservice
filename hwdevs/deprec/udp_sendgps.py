#!/usr/bin/env python3
#myservice_description: gps op

#sudo gpsd /dev/ttyACM0 -n -F /var/run/gpsd.sock

import gpsd
import requests
import datetime


from staticmap import StaticMap,CircleMarker
from fire import  Fire

import os
import socket


def get_address(lat,lon):
    try:
        res= requests.get(f'http://wttr.in/{lat},{lon}', timeout=3)
        addr= res.text[res.text.find("Location")+10:]
        addr = addr[:addr.find("[")]
        return addr
    except Exception as e:
        print(e)
        return None


#def downl_map(lon,lat, last_map_rended):
def downl_map(lat,lon, last_map_rended):
    """
    I had to switch lat, lon,  CircleMarker needs lon,lat
    """
    #----------------generate map here......
    now=datetime.datetime.now()
    dif=(now-last_map_rended).total_seconds()
    print("i... time to render map: {}".format(60-dif) )
    print("i... {} {}".format( lon,lat) )
    if dif>60:
        m = StaticMap(800, 800, 12)
        marker = CircleMarker( (lon, lat), '#0036FF', 12)
        m.add_marker(marker)
        #m.add_line(Line(((13.4, 52.5), (2.3, 48.9)), 'blue', 3))
        image = m.render(zoom=10)
        image.save('map11.png')
        image = m.render(zoom=13)
        image.save('map14.png')
        image = m.render(zoom=17)
        image.save('map17.png')
        last_map_rended=now
        print("i... map rendered")


def get_report_ip():
    config="~/.gps_rep_server"
    try:
        with open(os.path.expanduser(config)) as f:
            addrport=f.read()
            print("i... udp will go to:",addrport)
    except:
        print("X... no file ",config)
        addrport="127.0.0.1:50005"
    return addrport


def send_udp(lat, lon, address):
    UDP_IP, UDP_PORT=address.split(":")
    MESSAGE="{} {}".format(lat, lon)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    sock.sendto(bytes(MESSAGE, "utf-8"), (UDP_IP, int(UDP_PORT)))
    print("D... sent {} to {}:{}".format(MESSAGE,UDP_IP, UDP_PORT))
    #time.sleep(1)



def main(local=False):
    #UDP_IP = "127.0.0.1"
    #UDP_PORT = 50005
    #MESSAGE = "189"

    if local:
        address="127.0.0.1:50005"
    else:
        address = get_report_ip()

    r=gpsd.connect()
    print("/{}/".format(r))
    last_map_rended=datetime.datetime.now()
    last_map_rended=last_map_rended-datetime.timedelta(seconds = 60)

    # Get gps position
    ok = False
    try:
        p = gpsd.get_current()
        print(p.mode)
        ok = True
    except:
        ok = False
    if not ok:
        print("X... no read from gps ... ")
        print(" ...try\n ... sudo gpsd /dev/ttyACM0 -n -F /var/run/gpsd.sock")
    else:
        if p.mode>=2:
            pos=p.position()
            print(pos, p.position_precision() )
            print(p.map_url())
            print(get_address(*pos))
            # downl_map(*pos, last_map_rended)
            # -------------- UDP HERE ----------
            send_udp(*pos, address)

        else:
            print("o... not 2D or 3D fix yet ...")
            # See the inline docs for GpsResponse for the available data
    #print(packet.position())



if __name__=="__main__":
    Fire()
