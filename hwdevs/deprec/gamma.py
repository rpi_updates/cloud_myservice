#!/usr/bin/env python3
#myservice_description: RPI gamma detector
"""
https://raw.githubusercontent.com/r21m/Arduino-GDK101-gamma-sensor/master/GDK101datasheet_v1.5.pdf
https://www.amazon.de/FTLab-GDK101-GAMMA-Strahlung-Sensor-Modul/dp/B01I4RZWA8
https://github.com/r21m/Arduino-GDK101-gamma-sensor/blob/master/AN_GDK101_V1.0_I2C.pdf
https://raw.githubusercontent.com/r21m/Arduino-GDK101-gamma-sensor/master/AN_GDK101_V1.0_I2C.pdf


"""
from smbus2 import  SMBus
from tdb_io.influx import influxwrite
import time
from fire import Fire
import datetime as dt

def one_read( oldsec = 0, meter = 1):
    """
    olsec is for write on 59 sec.; meter is 1 in gamma_1 influx-field
    """
    bus = SMBus(1)

    # read time
    b = bus.read_word_data(0x18, 0xb1 )
    mins = b&0xff
    secs = b>>8

    # read 10min avg
    b = bus.read_word_data(0x18, 0xb2 )
    d10int = b&0xff
    d10dec = b>>8
    d10 = float( f"{d10int}.{d10dec}" )

    # read 1min avg
    b = bus.read_word_data(0x18, 0xb3 )
    d1int = b&0xff
    d1dec = b>>8
    d1 = float(f"{d1int}.{d1dec}")

    bus.close()

    newline = "\r"
    if oldsec > secs:
        newline="\n"
        values = f"dose10m={d10},dose1m={d1}"
        influxwrite("test",f"gamma_{meter}", values=values, DEBUG = True)
    print(f"{mins:02d}:{secs:02d}  ", end="")
    print(f"{d10:7.2f} uSv/h (10min avg)", end="")
    print(f"{d1:7.2f} uSv/h (1min avg)", end=newline)

    return secs

def main():
    secs = 0
    oldsec = 0
    while True:
        print( dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S") ," - ",end="")
        secs = one_read( oldsec)
        oldsec = secs
        time.sleep(1)

if __name__ == "__main__":
    Fire(main)
