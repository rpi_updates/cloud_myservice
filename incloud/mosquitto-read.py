#!/usr/bin/env python3
#myservice_description: read mosquito
import paho.mqtt.client as mqtt
import time
from tdb_io.influx import influxwrite
import json

def on_message(client, userdata, message):
    msgdec = str( message.payload.decode("utf-8") )
    if len(msgdec)<12: return
    print(f"received  message: " , msgdec )
    a = msgdec.split(" ")[-1]
    a = json.loads( a )
    print( "should be dict:",type(a) )
    lat = a["lat"]
    lon = a["lon"]
    alt = a["alt"]
    id = a["tid"]
    values =  f"geo{id}lat={lat},geo{id}lon={lon},geo{id}alt={alt}"
    influxwrite("test","geo",values=values)

mqttBroker ="127.0.0.1"
client = mqtt.Client("owntracks")
client.username_pw_set("ojr", "ojrojr")
client.connect(mqttBroker, port=18883)

client.loop_start()
client.subscribe("owntracks/#")
client.on_message=on_message

time.sleep(3600)
client.loop_stop()
