* What should be on raspberry:


┌─────────────────────────┬─────────┬───────────────┬──────────────────────────────────────────┐
│ service                 │ mode    │ AGE           │                                          │
├─────────────────────────┼─────────┼───────────────┼──────────────────────────────────────────┤
│* hwdevs/gps2seread       │ perm5m  │ 00:18:25      │                                          │
│* hwdevs/rpihat_hours     │ perm2m  │ 00:20:05      │  HAT every minute                        │
│* hwdevs/seread           │ stop    │ 02:18:22      │  read ttyUSB0 arduino                    │
│* image/flashcam          │ perm1m  │ 00:19:16      │  prx:opencvFlashCam                      │
│ image/pikrell           │ stop    │ 07:20:48 201d │                                          │
│* inet/ap_switch          │ perm1m  │ 00:18:58      │  Switch 2 StrongerAP                     │
│* inet/discover8086       │ perm5m  │ 00:23:11      │  searches for Influx:8086,8000,3000,5678 │
│* inet/piaddr.py          │ perm30s │ 00:18:27      │  change IP from hostname                 │
│+ inet/wlanext            │ stop    │ 11:14:13 201d │  pix4;SWITCH EXT ANTENA                  │
│* runonstart/noscrsave    │ perm2m  │ 00:19:06      │                                          │
│* runonstart/rpi_upgrades │ perm4m  │ 00:18:25      │  download update code                    │
│* runonstart/tag_influx   │ perm5m  │ 02:24:29      │  delete/create i_am_db                   │
└─────────────────────────┴─────────┴───────────────┴──────────────────────────────────────────┘
